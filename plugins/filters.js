import Vue from 'vue'

Vue.filter('price', (value) => {
  if (!value) {
    return ''
  }
  const intPart = Math.floor(value)
  let decimalPart = Math.round((value - intPart) * 100)

  if (decimalPart > 0 && decimalPart < 10) {
    decimalPart = ',0' + decimalPart
  } else if (decimalPart > 0 && decimalPart >= 10) {
    decimalPart = ',' + decimalPart
  } else {
    decimalPart = ',00'
  }
  return intPart + decimalPart + '€'
})
