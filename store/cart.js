export const state = () => ({
  products: []
})

export const mutations = {
  pushProductToCart (state, { id }) {
    // If product not added to cart, add it to cart
    if (!state.products.some(product => product.id === id)) {
      state.products.push({ id })
    }
  },
  removeProductFromCart (state, { id }) {
    // Assign new array without select product
    state.products = state.products.filter((product) => {
      return product.id !== id
    })
  },
  clearCartProducts (state) {
    state.products = []
  }
}

export const getters = {
  // Return current cart products or checkout products
  getCartProducts: (state, getters, rootState, rootGetters) => {
    return state.products.map(({ id }) => {
      // Find products in augmented products getter
      return rootGetters['products/augmentedProducts']().find(product => product.id === id)
    })
  },
  // Return cart products number
  getCartProductsNumber: (state) => {
    return state.products.length
  },
  // Return cart or checkout total price
  getCartTotalPrice: (state, getters) => {
    return getters.getCartProducts.reduce((total, product) => {
      return total + product.price
    }, 0)
  }
}
