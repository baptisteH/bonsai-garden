export const state = () => ({
  isLoggedIn: false,
  username: null
})

export const mutations = {
  login (state, username) {
    state.isLoggedIn = true
    state.username = username
  },
  logout (state) {
    state.isLoggedIn = false
    state.user = {}
  }
}
