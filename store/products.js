import { species } from '~/data/species'

export const state = () => ({
  products: [
    {
      id: 1,
      species_id: 1,
      age: 15,
      height: 22,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328173765/800x600x2/genevrier-chinois-itoigawa-22-cm-15-ans.jpg',
      price: 650
    },
    {
      id: 2,
      species_id: 2,
      age: 15,
      height: 22.2,
      img: 'https://cdn.webshopapp.com/shops/21919/files/330473670/800x600x2/pin-blanc-du-japon-222-cm-15-ans.jpg',
      price: 129.95
    },
    {
      id: 3,
      species_id: 1,
      age: 35,
      height: 32,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328252696/800x600x2/genevrier-de-chine-itoigawa-32-cm-35-ans-avec-un-b.jpg',
      price: 2900
    },
    {
      id: 4,
      species_id: 3,
      age: 35,
      height: 80,
      img: 'https://cdn.webshopapp.com/shops/21919/files/323647525/800x600x2/erable-japonais-80-cm-35-ans-dans-un-pot-yamafusa.jpg',
      price: 3675
    },
    {
      id: 5,
      species_id: 2,
      age: 15,
      height: 22.2,
      img: 'https://cdn.webshopapp.com/shops/21919/files/330473670/800x600x2/pin-blanc-du-japon-222-cm-15-ans.jpg',
      price: 129.95
    },
    {
      id: 6,
      species_id: 1,
      age: 35,
      height: 32,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328252696/800x600x2/genevrier-de-chine-itoigawa-32-cm-35-ans-avec-un-b.jpg',
      price: 2900
    },
    {
      id: 7,
      species_id: 3,
      age: 35,
      height: 80,
      img: 'https://cdn.webshopapp.com/shops/21919/files/323647525/800x600x2/erable-japonais-80-cm-35-ans-dans-un-pot-yamafusa.jpg',
      price: 3675
    },
    {
      id: 8,
      species_id: 1,
      age: 15,
      height: 22,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328173765/800x600x2/genevrier-chinois-itoigawa-22-cm-15-ans.jpg',
      price: 650
    },
    {
      id: 9,
      species_id: 2,
      age: 15,
      height: 22.2,
      img: 'https://cdn.webshopapp.com/shops/21919/files/330473670/800x600x2/pin-blanc-du-japon-222-cm-15-ans.jpg',
      price: 129.95
    },
    {
      id: 10,
      species_id: 1,
      age: 35,
      height: 32,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328252696/800x600x2/genevrier-de-chine-itoigawa-32-cm-35-ans-avec-un-b.jpg',
      price: 2900
    },
    {
      id: 11,
      species_id: 3,
      age: 35,
      height: 80,
      img: 'https://cdn.webshopapp.com/shops/21919/files/323647525/800x600x2/erable-japonais-80-cm-35-ans-dans-un-pot-yamafusa.jpg',
      price: 3675
    },
    {
      id: 12,
      species_id: 2,
      age: 15,
      height: 22.2,
      img: 'https://cdn.webshopapp.com/shops/21919/files/330473670/800x600x2/pin-blanc-du-japon-222-cm-15-ans.jpg',
      price: 129.95
    },
    {
      id: 13,
      species_id: 1,
      age: 35,
      height: 32,
      img: 'https://cdn.webshopapp.com/shops/21919/files/328252696/800x600x2/genevrier-de-chine-itoigawa-32-cm-35-ans-avec-un-b.jpg',
      price: 2900
    },
    {
      id: 14,
      species_id: 3,
      age: 35,
      height: 80,
      img: 'https://cdn.webshopapp.com/shops/21919/files/323647525/800x600x2/erable-japonais-80-cm-35-ans-dans-un-pot-yamafusa.jpg',
      price: 3675
    }
  ]
})

export const getters = {
  // Add properties to each store product
  augmentedProducts: state => (itemsNumber = null) => {
    let augmentedProducts = state.products.map((product) => {
      // Add species name from imported const to bonsai item
      const augmentedProduct = {
        ...product,
        species_name: species.find(({ id }) => id === product.species_id).name
      }
      // Bonsai item name format
      const age = augmentedProduct.age + ' ans'
      const height = augmentedProduct.height + ' cm'
      augmentedProduct.shortName = 'Bonsaï ' + augmentedProduct.species_name
      augmentedProduct.name = augmentedProduct.shortName + ', ' + age + ', ' + height
      return augmentedProduct
    })

    // If limited number of items specified, truncate array
    augmentedProducts = itemsNumber !== null ? augmentedProducts.slice(0, itemsNumber) : augmentedProducts

    return augmentedProducts
  },
  // Return products price range
  priceRangeLimits (state) {
    // Get all products prices
    const prices = state.products.map((product) => {
      return product.price
    })
    // Get min and max price from prices
    // Use floor and ceil to prevent decimal prices out of bounds
    const min = Math.floor(Math.min.apply(Math, prices))
    const max = Math.ceil(Math.max.apply(Math, prices))
    return [min, max]
  },
  // Return products size range
  heightRangeLimits (state) {
    // Get all products sizes
    const heights = state.products.map((product) => {
      return product.height
    })
    // Get min and max height from heights
    const min = Math.floor(Math.min.apply(Math, heights))
    const max = Math.ceil(Math.max.apply(Math, heights))
    return [min, max]
  }
}

export const actions = {
  getProductById ({ commit, state, getters }, { id }) {
    const products = getters.augmentedProducts()
    return products.find(product => product.id === parseInt(id))
  }
}
