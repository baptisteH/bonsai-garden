export const species = [
  {
    id: 1,
    name: 'Genévrier chinois'
  },
  {
    id: 2,
    name: 'Pin blanc du Japon'
  },
  {
    id: 3,
    name: 'Érable japonais'
  }
]
